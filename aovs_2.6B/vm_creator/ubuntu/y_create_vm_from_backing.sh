#!/bin/bash

script_dir="$(dirname $(readlink -f $0))"
IVG_dir="/root/ivg"
VM_disk_size="15G"

# Shutdown all VMs
$IVG_dir/helper_scripts/delete-vms.sh --all --shutdown
# Undefine Netronome VMs
$IVG_dir/helper_scripts/delete-vms.sh --filter "netronome"

LIBVIRT_DIR=/var/lib/libvirt/images
UBUNTU_CLOUD_IMAGE=ubuntu-18.04-server-cloudimg-amd64.img
basefile=$LIBVIRT_DIR/$UBUNTU_CLOUD_IMAGE

# Check if VM name is passed
if [ -z "$1" ]; then
   echo "ERROR: Please pass a VM name as the first parameter of this script..."
   exit -1
   else
   VM_NAME=$1
fi

cat <<- EOF > /tmp/ifcfg-eth0
DEVICE="eth0"
ONBOOT="yes"
IPV6INIT="no"
BOOTPROTO="dhcp"
TYPE="Ethernet"
EOF

# Check for and install 'libguestfs-tools'
$IVG_dir/helper_scripts/install-packages.sh \
    "guestfish@libguestfs-tools" \
    || exit -1

echo "create overlay image"
overlay="$LIBVIRT_DIR/$VM_NAME.qcow2"
qemu-img create -b $basefile -f qcow2 $overlay $VM_disk_size\
    || exit -1
sleep 5
guestfish --rw -i -a $overlay write /etc/hostname $VM_NAME \
    || exit -1

echo "create domain"

cpu_model=$(virsh capabilities \
    | grep -o '<model>.*</model>' \
    | head -1 \
    | sed 's/\(<model>\|<\/model>\)//g')


virt-install \
    --name "$VM_NAME" \
    --disk path=${overlay},format=qcow2,bus=virtio,cache=none \
    --ram 2048 \
    --vcpus 2 \
    --cpu $cpu_model \
    --network bridge=virbr0,model=virtio \
    --graphics none \
    --debug \
    --accelerate \
    --os-type=linux \
    --os-variant=ubuntu16.04 \
    --noautoconsole \
    --noreboot \
    --import \
    || exit -1

virsh start $VM_NAME


#echo "determine guest IP address"
#while : ; do
#    sleep 2
#    echo -n "."
#    counter=$((counter + 1))
#    ipaddr=$(arp -an | grep $(virsh dumpxml $VM_NAME | awk -F\' '/mac address/ {print $2}') | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}')
#    if [ ! -z "$ipaddr" ]
#    then
#        echo ""
#        echo "IP address was determined"
#        break
#    fi
#    if [ $counter -gt 30 ]; then
#        echo "ERROR: VM failed to start up"
#        exit -1
#    fi
#done
#echo "ipaddr = $ipaddr"
#sleep 10
#

#move second header in partition table, delete old partition, create new partition
#echo -e "${GREEN}Reconfiguring the partion table${NC}"
#ssh -o "StrictHostKeyChecking no" root@$ipaddr \
#"sgdisk -e /dev/vda" \
#"sgdisk -d=1 /dev/vda" \
#"sgdisk -n=1:0:0 /dev/vda" \
#"exit"
##"poweroff -f"
##"shutdown -P now"

#echo "reboot guest"
#virsh destroy $VM_NAME
#sleep 5
#virsh start $VM_NAME


#echo "determine guest IP address"
#while : ; do
#    sleep 2
#    echo -n "."
#    counter=$((counter + 1))
#    ipaddr=$(arp -an | grep $(virsh dumpxml $VM_NAME | awk -F\' '/mac address/ {print $2}') | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}')
#    if [ ! -z "$ipaddr" ]
#    then
#        echo ""
#        echo "IP address was determined"
#        break
#    fi
#    if [ $counter -gt 30 ]; then
#        echo "ERROR: VM failed to start up"
#        exit -1
#    fi
#done
#echo "ipaddr = $ipaddr"
#sleep 15

#Remove VM IP from known Hosts if present
#ssh-keygen -R $ipaddr

#sleep 10

#increase file system size
#echo "increase file system size"
#ssh -o "StrictHostKeyChecking no" root@$ipaddr \
#"resize2fs /dev/vda1"

echo "VM has been created!"
exit 0
