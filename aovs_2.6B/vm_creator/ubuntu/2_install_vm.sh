#!/bin/bash

#Install backing VM
VM_NAME=ubuntu_backing
UBUNTU_CLOUD_IMAGE=ubuntu-18.04-server-cloudimg-amd64.img
IVG_dir="/root/ivg"

cpu_model=$(virsh capabilities | grep -o '<model>.*</model>' | head -1 | sed 's/\(<model>\|<\/model>\)//g')

# Shutdown/undefine previous VM with the same name
$IVG_dir/helper_scripts/delete-vms.sh --name "$VM_NAME" \
     || exit -1

virt-install \
  --name $VM_NAME \
  --disk path=/var/lib/libvirt/images/$UBUNTU_CLOUD_IMAGE,format=qcow2,bus=virtio,cache=none \
  --disk /var/lib/libvirt/images/user_data_1.img,device=cdrom \
  --ram 4096 \
  --vcpus 4 \
  --cpu $cpu_model \
  --network bridge=virbr0,model=virtio \
  --graphics none \
  --debug \
  --accelerate \
  --os-type=linux \
  --os-variant=ubuntu16.04 \
  --noautoconsole \
  --import \
  || exit -1

# --network bridge=br0,virtualport_type="openvswitch",virtualport_profileid="vport0" \
# --network bridge=br0,model=virtio,virtualport_type="openvswitch" \

#wait for VM to shutdown
echo -n "Waiting for VM to shutdown"
while : ; do
    state=$(virsh list --all \
        | grep $VM_NAME \
        | awk '{print $3}')
    if [ "$state" != "running" ]; then
        break
    fi
    sleep 1
    echo -n "."
done

echo " DOWN"

sleep 1

#Eject user_data_1.img
virsh change-media $VM_NAME \
    /var/lib/libvirt/images/user_data_1.img \
    --eject --config \
    || exit -1

exit 0

