#!/bin/bash

#Start VM

VM_NAME="ubuntu_backing"
script_dir="$(dirname $(readlink -f $0))"

virsh start $VM_NAME \
    || exit -1

echo -n "Waiting for VM to boot "
counter=0

while : ; do
    sleep 2
    echo -n "."
    counter=$((counter + 1))
    #macaddr=$(virsh dumpxml $VM_NAME | grep "mac address" | awk -F\' '{ print $2}')
    #ipaddr=$(arp -an | grep $macaddr)
    ipaddr=$(arp -an | grep $(virsh dumpxml $VM_NAME | awk -F\' '/mac address/ {print $2}') | egrep -o '([0-9]{1,3}\.){3}[0-9]{1,3}')
    if [ ! -z "$ipaddr" ]
    then
        echo "\nIP address was determined"
        break
    fi
    if [ $counter -gt 30 ]; then
        echo "ERROR: VM failed to start up"
        exit -1
    fi
done
echo "ipaddr = $ipaddr"
sleep 10

echo
echo "VM IP address: $ipaddr"

echo "Copying setup scripts to VM..."

#Remove VM IP from known Hosts if present
ssh-keygen -R $ipaddr

sleep 10

#Copy Setup scripts to VM
scp -o StrictHostKeyChecking=no \
    -r $script_dir/vm_scripts \
    root@$ipaddr:/root/ \
    || exit -1

exit 0
